/*
	Activity:


	1. Debug the following code and avoid the errors.
	2. Create a function which is able to receive 2 numbers as arguments.
		-Add 2 parameters to receive the arguments.
		-Show the sum of both numbers in the console.

*/

//1.Debug the following code and avoid the errors:
	//check the code for errors.
		//delete erratic syntax.
		//comment out erratic statements.
		//all console logs must work as intended in the output.

		// Check your Boodle Notes to get the code to be debugged.



//2.Create a function named getSum() which is able to receive 2 numbers as arguments.
/*-Add 2 parameters to receive the arguments.
-Show the sum of both numbers in the console.*/

// For #1
let fullname = "Steve Rogers";
console.log(fullname);

let age = 40;
console.log(age);

let friends = ["Tony","Bruce","Thor","Natasha","Clint","Nick"];
console.log(friends);

let profile = {

	username: "captain_america",
	fullName: "Steve Rogers",
	age: 40,
	isActive: false,

}

console.log(profile);

fullname = "Tony Stark";
console.log(fullname);

let largestOcean = "Pacific Ocean";
largestOcean = "Atlantic Ocean";
console.log(largestOcean);


// For #2
let getSum = (a, b) => console.log(a + b);
getSum(25, 25);